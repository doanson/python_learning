from django.db import models


# Create your models here.
class Agent(models.Model):
    agent_code = models.CharField(max_length=6, primary_key=True)
    agent_name = models.CharField(max_length=40)
    working_area = models.CharField(max_length=35)
    commission = models.DecimalField(max_digits=10, decimal_places=2)
    phone_no = models.CharField(max_length=15)
    country = models.CharField(max_length=25)


class Customer(models.Model):
    cust_code = models.CharField(max_length=6, primary_key=True)
    cust_name = models.CharField(max_length=40)
    cust_city = models.CharField(max_length=35)
    working_area = models.CharField(max_length=35)
    cust_country = models.CharField(max_length=20)
    grade = models.IntegerField()
    opening_amt = models.CharField(max_length=6)
    receive_amt = models.CharField(max_length=6)
    payment_amt = models.CharField(max_length=6)
    outstanding_amt = models.CharField(max_length=6)
    phone_no = models.CharField(max_length=6)
    agent = models.ForeignKey(Agent, on_delete=models.CASCADE)


class Order(models.Model):
    ord_num = models.DecimalField(max_digits=6, decimal_places=0, primary_key=True)
    ord_amount = models.DecimalField(max_digits=12, decimal_places=2)
    advance_amount = models.DecimalField(max_digits=12, decimal_places=2)
    ord_date = models.DateField()
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    agent = models.ForeignKey(Agent, on_delete=models.CASCADE)
    ord_description = models.CharField(max_length=60)



