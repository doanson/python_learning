from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('total/', views.total_orders, name="total_order"),
    path('granularity/', views.granularity, name="granularity"),
    path('customer/', views.customer, name="customer"),
    path('date-range/', views.date_range, name="date-range")
]
