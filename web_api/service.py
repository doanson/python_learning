from web_api.models import Agent, Customer, Order
from io import BytesIO as IO
import pandas as pd
import re
import pendulum
import xlsxwriter
import ast


def count_order():
    return Order.objects.all().count()


def query_data(granu, group_by):
    group_by_att = f'customer__{group_by}'

    data = Order.objects.values('ord_date', 'customer_id', group_by_att).order_by('ord_date')
    df = pd.DataFrame(data)
    df = df.set_index(['ord_date'])
    df.index = pd.to_datetime(df.index)

    if granu == 'daily':
        df = df.groupby([pd.Grouper(freq='D'), df[group_by_att]])[group_by_att].count().reset_index(name="Order Number")
        df.columns = format_header(df.columns.tolist())
        print(df)
        return df
        # return Order.objects.values('customer', group_by_att, 'ord_date') \
        #     .annotate(order_amount=Count('customer_id')).order_by('ord_date')
    elif granu == 'weekly':
        df = df.groupby([pd.Grouper(freq='W-MON'), df[group_by_att]])[group_by_att] \
            .count().reset_index(name="Order Number")
        df['week number'] = pd.to_datetime(df['ord_date']).dt.week
        df['Start/End Of Week'] = get_start_end_of_specific_date(df['ord_date'].tolist())
        df.columns = format_header(df.columns.tolist())
        print(df)
        return df
    if granu == 'monthly':
        df = df.groupby([pd.Grouper(freq='M'), df[group_by_att]])[group_by_att].count().reset_index(name="Order Number")
        print(df.columns)
        df.columns = format_header(df.columns.tolist())
        print(df.columns)
        return df


def write_to_excel(data_frame, format=False):
    excel_file = IO()
    xlwriter = pd.ExcelWriter(excel_file, engine='xlsxwriter')
    data_frame.to_excel(xlwriter, 'sheet01', index=False)

    worksheet = xlwriter.sheets['sheet01']
    workbook = xlwriter.book

    # Set row height
    worksheet.set_default_row(20)
    max_column_length = longest_cell(data_frame)
    worksheet.set_column(0, len(data_frame.columns), max_column_length)

    if format:
        format_excel_table(workbook, worksheet, data_frame)

    xlwriter.save()
    xlwriter.close()
    excel_file.seek(0)
    return excel_file


def format_header(arr_header):
    pattern = 'customer__cust_'
    replace = ''
    arr_header[0] = "Order Date"

    for index, value in enumerate(arr_header):
        arr_header[index] = re.sub(pattern, replace, value).capitalize()

    return arr_header


def remove_underscore(arr_header):
    print('remove underscore')
    for index, value in enumerate(arr_header):
        arr_header[index] = re.sub('_', " ", value).capitalize()
    return arr_header


def get_start_end_of_specific_date(data):
    for index, value in enumerate(data):
        dt = pendulum.datetime(value.year, value.month, value.day)
        dt.format("YYYY-MM-DD")
        data[index] = f"{dt.start_of('week').to_date_string()} -- {dt.end_of('week').to_date_string()}"
    return data


def style_table(df_html):
    default_formatter = "<!DOCTYPE html>" \
                        "<html>" \
                        "<head>" \
                        "<title>Page Title</title>" \
                        "<style>" \
                        "body { text-align: left;color: black;" \
                        "font-family: Arial, Helvetica, sans-serif;" \
                        "font-size:12px;}" \
                        "body>div: {width: 50%;}" \
                        "body>div>table { border-spacing: initial; " \
                        "width: 100%;" \
                        "border-collapse: collapse;" \
                        "border-right: 1px solid #ddd;" \
                        "border-bottom: 1px solid #ddd;}" \
                        "body>div>table>thead>tr>th { border-bottom: 5px solid #ddd; text-align: left; border-right: 1px solid #ddd;}" \
                        "body>div>table>tbody>tr:last-child { background-color: #BFBFBF;}" \
                        "body>div>table>tbody>tr>th,td{border-right: 1px solid #ddd;}" \
                        "</style>" \
                        "</head>" \
                        f"<body><div>{df_html}</div></body>" \
                        "</html>"
    return default_formatter


def get_customers(ftype='excel', filter_data='all'):
    if filter_data == 'all':
        data = Order.objects.values('ord_date', 'customer_id', 'ord_amount')
    else:
        filter_data = ast.literal_eval(filter_data)
        data = Order.objects.filter(customer__cust_code__in=filter_data).values()

    result = caculate_and_export(data, ftype)
    return result


def query_by_date(start_date, end_date, ftype='excel'):
    data = Order.objects.filter(ord_date__range=[start_date, end_date]).values()
    result = caculate_and_export(data, ftype)
    return result


def format_excel_table(workbook, worksheet, data_frame):
    worksheet.hide_gridlines(2)
    border_fmt = workbook.add_format({'right': 1})
    background_format = workbook.add_format({'bg_color': 'red'})
    header_border = workbook.add_format({'bottom': 1, 'top': 0, 'left': 0, 'right': 1})
    # Add border for header
    worksheet.conditional_format(xlsxwriter.utility.xl_range(0, 0, 0, len(data_frame.columns) - 1),
                                 {'type': 'no_errors', 'format': header_border})

    # Add split line between two columns
    worksheet.conditional_format(xlsxwriter.utility.xl_range(0, 0, len(data_frame) - 1, len(data_frame.columns) - 1),
                                 {'type': 'no_errors', 'format': border_fmt})
    # Add background color to the "total" line
    worksheet.conditional_format(
        xlsxwriter.utility.xl_range(len(data_frame), 0, len(data_frame), len(data_frame.columns) - 1),
        {'type': 'no_errors', 'format': background_format})


def caculate_and_export(data_frame, ftype):
    df = pd.DataFrame(data_frame)
    df = df.groupby(['customer_id'])['ord_amount'].sum().reset_index(name='order_amount')
    sum_order = df['order_amount'].sum()
    new_df = pd.DataFrame({
        'customer_id': ['Total'],
        'order_amount': [sum_order]
    })
    df = df.append(new_df)
    df['order_amount'] = 'USD ' + df['order_amount'].astype(str)
    df.columns = remove_underscore(df.columns.tolist())

    if ftype == 'excel':
        return df

    html = df.to_html(index=False).replace('border="1"', '')
    format_html = style_table(html)
    return format_html


def longest_cell(data_frame):
    for i, col in enumerate(data_frame.columns):
        column_len = data_frame[col].astype(str).str.len().max()
        column_len = max(column_len, len(col))
    return column_len
