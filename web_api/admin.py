from django.contrib import admin
from web_api.models import Agent, Order, Customer


# Register your models here.


class AgentsAdmin(admin.ModelAdmin):
    list_display = ('agent_code', 'agent_name', 'working_area', 'commission', 'phone_no', 'country')


class CustomersAdmin(admin.ModelAdmin):
    list_display = ('cust_code', 'cust_name', 'cust_city', 'working_area', 'cust_country', 'grade',
                    'opening_amt', 'receive_amt', 'payment_amt', 'outstanding_amt', 'phone_no',
                    'agent')


class OrdersAdmin(admin.ModelAdmin):
    list_display = ('ord_num', 'ord_amount', 'advance_amount', 'ord_date', 'customer', 'agent',
                    'ord_description')


admin.site.register(Agent, AgentsAdmin)
admin.site.register(Customer, CustomersAdmin)
admin.site.register(Order, OrdersAdmin)
