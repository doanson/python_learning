from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from web_api.service import count_order, query_data, write_to_excel, get_customers, query_by_date
import pandas as pd
import time
import json
from dateutil.parser import parse

# Create your views here.


def index(request):
    return HttpResponse('This is index page')


def total_orders(request):
    num = count_order()
    return HttpResponse(f'Total number of orders is: {num}')


def granularity(request):
    # data = request.POST.dict()
    data = json.loads(request.body)

    error_dict = {
        "msg": "Please passs granularity, group_by",
        "status": 500
    }
    if not bool(data):
        return JsonResponse(error_dict)
    elif not data.get('granularity') or not data.get('group_by'):
        return JsonResponse(error_dict)

    granu = data['granularity']
    group = data['group_by']
    result = query_data(granu, group)
    df = pd.DataFrame(result)

    excel_file = write_to_excel(df)
    filename = f"{time.time()}_{group}_{granu}_report"

    response = HttpResponse(excel_file.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = f'attachment; filename={filename}.xlsx'
    return response


def customer(request):
    content = json.loads(request.body)
    type = content['support_type']
    fiter_data = content['customer_ids']

    data = get_customers(type, fiter_data)

    if type == 'excel':
        excel_file = write_to_excel(data, True)
        filename = f"{time.time()}_customer_report"

        response = HttpResponse(excel_file.read(),
                                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = f'attachment; filename={filename}.xlsx'
        return response

    return HttpResponse(data)


def date_range(request):
    data = json.loads(request.body)
    error_dict = {
        "msg": "Please pass start_date, end_date",
        "status": 500
    }

    if not bool(data):
        return JsonResponse(error_dict)
    elif not data.get('start_date') or not data.get('end_date'):
        return JsonResponse(error_dict)

    start_date = data['start_date']
    end_date = data['end_date']
    ftype = data['support_type']

    if not is_date(start_date) or not is_date(end_date):
        return JsonResponse({
            "msg": "Please check date format",
            "status": "500"
        })
    data = query_by_date(start_date, end_date, ftype)

    if ftype == 'excel':
        excel_file = write_to_excel(data, True)
        filename = f"{start_date}_{end_date}_report"

        response = HttpResponse(excel_file.read(),
                                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = f'attachment; filename={filename}.xlsx'
        return response

    return HttpResponse(data)


def is_date(string):
    try:
        parse(string)
        return True
    except ValueError:
        return False
