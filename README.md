## Python exercise
Dev env:
- Python: 3.8
- Postgres: 
- Pandas: 

## Installation

#### Clone the code
```
git clone https://gitlab.com/doanson/python_learning.git
```
#### Create virtual enviroment
```
virtualenv my_env
```
#### Activate virtualenv
```
source my_env/bin/activate
```
#### Change directory
```
cd python_learning
```

#### Install virtual packages
```
pip install -r requirements.txt
```
#### Create database name test_db
```
sudo -u postgres psql
CREATE DATABASE test_db;
```
#### Create a new user
```
CREATE USER test_user WITH PASSWORD 'test_user';

```
#### Grant all permission of test_db
```
GRANT ALL ON DATABASE test_db TO test_user
```
#### Run migrations
```
python3 manage.py makemigrations
```
### Run migrate
```
python3 manage.py migrate
```
#### Seed data
```
python3 manage.py loaddata web_api/seed_data/dump_data.json
```
#### Start the app
```
python3 manage.py runserver
```
#### Request by PostMan
Url: Reporting
```
http://127.0.0.1:8000/api/granularity/
```
Method: POST
Body: 
```
"granularity": "weekly"
"group_by": "cust_country"
```
##### Possible granularity: weekly, monthly, daily
##### Possible group_by: cust_name, cust_city, cust_country
Url: Get total number of Order
Method: GET
```
http://127.0.0.1:8000/api/granularity/
``` 
Url: Get customer/all 
```
http://127.0.0.1:8000/api/customer/
{
	"granularity": "granularity",
	"group_by": "cust_country",
	"support_type":"excel",
	"customer_ids": "['C00001', 'C0002']"
}
``` 
Url: Select customers with id/all 
Possible support typ: 'excel', 'html'
```
http://127.0.0.1:8000/api/customer/
{
	"granularity": "granularity",
	"group_by": "cust_country",
	"support_type":"excel",
	"customer_ids": "['C00001', 'C0002']"
}
``` 
Url: Select customers all 
```
http://127.0.0.1:8000/api/customer/
{
	"granularity": "granularity",
	"group_by": "cust_country",
	"support_type":"excel",
	"customer_ids": "all"
}
``` 
Url: Get customer by date-range
```
http://127.0.0.1:8000/api/date-range/
{
	"start_date": "2008-07-20",
	"end_date": "2008-08-01",
	"support_type": "excel"
}
``` 
